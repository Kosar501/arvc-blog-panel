import ArticlesList from "../../views/pages/articles/ArticlesList.vue";
import Article from "../../views/pages/articles/Article.vue";
export default [
  {
    path: "/articles/list",
    name: "articlesList",
    component: ArticlesList,
    meta: { title: "Articles", requiresAuth: true },
  },
  {
    path: "/articles/single/:slug?", // slug is optional
    name: "article",
    component: Article,
    meta: { title: "Article", requiresAuth: true },
  },
];
