import Login from "../../views/pages/auth/Login.vue";
import Register from "../../views/pages/auth/Register.vue";
export default [
  {
    path: "/",
    name: "login",
    component: Login,
    meta: {
      title: "Login",
    },
  },
  {
    path: "/register",
    name: "register",
    component: Register,
    meta: {
      title: "Register",
    },
  },
];
