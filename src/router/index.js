import Vue from "vue";
import VueRouter from "vue-router";
import Auth from "./modules/Auth";
import Articles from "./modules/Articles";
//modules
Vue.use(VueRouter);
const routes = [...Auth, ...Articles];
const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});
//midllware
router.beforeEach((to, from, next) => {
  //set title
  document.title = to.meta.title || "Blog Panel";
  //impelement auth gaurd
  if (to.meta.requiresAuth && !localStorage.getItem("token")) {
    next({
      name: "login",
    });
  } else if (
    localStorage.getItem("token") &&
    (to.name == "login" || to.name == "register")
  ) {
    next({
      name: "articlesList",
    });
  } else {
    next();
  }
});
export default router;
