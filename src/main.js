import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
//bootstrap && icons
import { BootstrapVue, BootstrapVueIcons } from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
//vue toast
import VueToast from "vue-toast-notification";
import "vue-toast-notification/dist/theme-sugar.css";
//vee validate
import VeeValidate from "vee-validate";
Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);
Vue.use(VueToast);
Vue.use(VeeValidate, {
  aria: true,
  fieldsBagName: "veeFields",
});
Vue.config.productionTip = false;
new Vue({
  router,
  store,
  render: (h) => h(App),
  methods: {},
}).$mount("#app");
