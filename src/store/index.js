import Vue from "vue";
import Vuex from "vuex";
import { axios } from "@/plugins/axios";
Vue.use(Vuex);
//plugins
const axiosPlugin = (store) => {
  store.$axios = axios;
};
import userStore from "./modules/user.store";
import articlesStore from "./modules/article.store";
import tagsStore from "./modules/tags.store";
export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: { userStore, articlesStore, tagsStore },
  plugins: [axiosPlugin],
});
