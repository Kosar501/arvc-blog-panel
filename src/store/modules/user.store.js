const url = process.env.VUE_APP_API_URL;
export default {
  state: {
    user: {},
  },
  getters: {
    getUser(state) {
      return state.user;
    },
    getUserToken() {
      return localStorage.getItem("token");
    },
  },
  mutations: {
    setUser(state, data) {
      state.user = data;
      if (typeof data.token != "undefined")
        localStorage.setItem("token", data.token);
    },
  },
  actions: {
    async register(context, { user }) {
      await this.$axios
        .post(url + "users", {
          user: {
            username: user.username,
            email: user.email,
            password: user.password,
          },
        })
        .then(function (response) {
          if (response.status === 200 || response.status === 201) {
            context.commit("setUser", response.data.user);
          }
        });
    },
    async login(context, { user }) {
      await this.$axios
        .post(url + "users/login", {
          user: {
            email: user.email,
            password: user.password,
          },
        })
        .then(function (response) {
          if (response.status === 200 || response.status === 201) {
            context.commit("setUser", response.data.user);
          }
        });
    },
    logout(context) {
      localStorage.removeItem("token");
      context.commit("setUser", {});
    },
    getCurrentUser(context) {
      this.$axios
        .get(url + "user", {
          headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
        })
        .then(function (response) {
          if (response.status === 200) {
            context.commit("setUser", response.data.user);
          }
        });
    },
  },
};
