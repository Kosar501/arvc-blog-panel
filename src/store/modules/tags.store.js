const url = process.env.VUE_APP_API_URL;
export default {
  state: {
    tags: [],
  },
  getters: {
    getTags(state) {
      return state.tags;
    },
  },
  mutations: {
    setTags(state, data) {
      state.tags = data;
    },
  },
  actions: {
    async getTagsReq(context) {
      await this.$axios.get(url + "tags").then(function (response) {
        if (response.status === 200) {
          if (response.data.tags) {
            context.commit("setTags", response.data.tags);
          }
        }
      });
    },
  },
};
