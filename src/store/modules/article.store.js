const url = process.env.VUE_APP_API_URL;
export default {
  state: {
    article: {},
    articles: [],
  },
  getters: {
    getArticle(state) {
      return state.article;
    },
    getArticles(state) {
      return state.articles;
    },
  },
  mutations: {
    setArticle(state, data) {
      state.article = data;
    },
    setArticles(state, data) {
      state.articles = data;
    },
  },
  actions: {
    async getArticlesReq(context, { page }) {
      let arUrl = page === 1 ? "articles" : "articles/page/?page=" + page;
      await this.$axios
        .get(url + arUrl, {
          headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
        })
        .then(function (response) {
          if (response.status === 200) {
            if (response.data.articles) {
              context.commit("setArticles", response.data.articles);
            }
          }
        });
    },
    async getArticleReq(context, { slug }) {
      await this.$axios
        .get(url + "articles/" + slug, {
          headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
        })
        .then(function (response) {
          if (response.status === 200) {
            if (response.data.article) {
              context.commit("setArticle", response.data.article);
            }
          }
        });
    },
    async createArticle(context, { article }) {
      await this.$axios
        .post(
          url + "articles",
          {
            article: article,
          },
          {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          }
        )
        .then(function (response) {
          if (response.status === 200) {
            context.commit("setArticle", response.data.article);
          }
        });
    },
    async updateArticle(context, { article }) {
      await this.$axios
        .put(
          url + "articles/" + article.slug,
          {
            article: article,
          },
          {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          }
        )
        .then(function (response) {
          if (response.status === 200) {
            context.commit("setArticle", response.data.article);
          }
        });
    },
    async deleteArticle(context, { slug }) {
      await this.$axios
        .delete(url + "articles/" + slug, {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Token ${localStorage.getItem("token")}`,
          },
        })
        .then(function (response) {
          if (response.status === 200) {
            //remove from array
            var arr = context.getters.getArticles;
            arr = arr.filter((obj) => obj.slug !== slug);
            context.commit("setArticles", arr);
          }
        });
    },
  },
};
