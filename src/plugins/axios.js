import Axios from "axios";
import Vue from "vue";
// insert all your axios logic here
export const axios = Axios;
//functions
const isEmpty = function (obj) {
  return Object.keys(obj).length === 0;
};
const parseErrors = function (errObject) {
  if (!errObject.errors) return "Something went wrong";
  var str = "";
  for (const [p, val] of Object.entries(errObject.errors)) {
    str += `${p} ${val}.<br>`;
  }
  return str;
};
// Add a response interceptor >> error handler
axios.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    if ([401].includes(error.response.status)) {
      if (error.response.data.message) {
        Vue.$toast.error(error.response.data.message, {
          position: "top-right",
        });
      } else {
        Vue.$toast.error("Unauthorized", {
          position: "top-right",
        });
      }
      //clear local storage
      localStorage.removeItem("token");
    }
    if ([404].includes(error.response.status)) {
      if (!isEmpty(error.response.data)) {
        Vue.$toast.error(parseErrors(error.response.data), {
          position: "top-right",
        });
      } else {
        Vue.$toast.error("NotFound", {
          position: "top-right",
        });
      }
    }
    if ([422].includes(error.response.status)) {
      if (!isEmpty(error.response.data)) {
        Vue.$toast.error(parseErrors(error.response.data), {
          position: "top-right",
        });
      } else {
        Vue.$toast.error("Unexpected error", {
          position: "top-right",
        });
      }
    }
    //other erros
    if ([403, 500].includes(error.response.status)) {
      if (error.response.data && error.response.data.message) {
        Vue.$toast.error(error.response.data.message, {
          position: "top-right",
        });
      } else {
        Vue.$toast.error(parseErrors(error.response.data), {
          position: "top-right",
        });
      }
    }
    return error;
  }
);
export default {
  install(Vue) {
    Vue.prototype.$axios = Axios;
  },
};
