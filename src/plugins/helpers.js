const parseErrors = function (errObject) {
  var str = "";
  for (const [p, val] of Object.entries(errObject.errors)) {
    str += `${p} ${val}\n`;
  }
  return str;
};
export default {
  parseErrors,
};
